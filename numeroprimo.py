"""                author: "Angel Morocho"
             email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 1:
            Escribe un numero y comprobar si es un numero primo:"""


def numero_primo(numero):
    contador = 0
    for i in range(1, numero + 1):
        if (numero % i) == 0:
            contador = contador + 1
        if contador >= 3:
            break
    if contador == 2:
        print("el numero es primo")
    else:
        print("el numero no es primo")


if __name__ == '__main__':
    while True:
        try:
            num = input("Ingrese el numero a comprobar: \n")
            num_comp = int(num)
            numero_primo(num_comp)
            break
        except ValueError:
            print("Incorrecto: Ingrese un numero entero")